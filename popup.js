document.addEventListener('DOMContentLoaded', function () {
    chrome.tabs.executeScript({
        file: 'myscript.js'
    }, async function () {
        chrome.storage.local.get(null, function (result) {
            if (result.photos == "") {
                var para = document.createElement("p");
                para.style.color = "white";
                para.style.width = "100px";
                para.style.fontSize = "large";
                para.style.textAlign = "center";
                var node = document.createTextNode("I found no photo to open");
                para.appendChild(node);
                var element = document.getElementById("1");
                element.appendChild(para);
                return;
            }
            //console.log(result);
            chrome.windows.getCurrent(function (w) {
                let div = document.getElementsByTagName("div")[0];
                if (w.width > 930) {
                    div.style.width = "760px";
                }
            });
            let photo = result.photos;
            let display = result.display;
            let urls = [];
            let urls_display = [];
            if (photo.indexOf(',') > 0) {
                urls = photo.split(',');
            }
            else {
                urls[0] = String(photo);
            }
            if (display.indexOf(',') > 0) {
                urls_display = display.split(',');
            }
            else {
                urls_display[0] = String(display);
            }
            for (let i = 0; i < urls.length; i++) {
                if (urls_display[i].indexOf("mp4") < 0) {
                    var para = document.createElement("img");
                    para.classList.add("img_class");
                    //para.src = urls_display[i];
                    para.src = "camera.png";
                    var element = document.getElementById("1");
                    element.appendChild(para);
                }
                else {
                    let container = document.createElement("div");
                    container.classList.add("video_thumbnail");
                    var para = document.createElement("video");
                    container.appendChild(para);
                    para.classList.add("img_class");
                    para.src = urls_display[i];
                    var element = document.getElementById("1");
                    element.appendChild(container);
                    container.addEventListener('click', function () {
                        window.open(urls[i])
                    });
                }


                para.addEventListener('click', function () {
                    window.open(urls[i])
                });
            }
        });
    })
});
